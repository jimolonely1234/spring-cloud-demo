package com.jimo.zipkin;

import brave.sampler.Sampler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * @author jimo
 * @code
 * @date 2018/9/8 18:45
 */
@SpringBootApplication
@RestController
public class ZipkinServiceHiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinServiceHiApplication.class, args);
    }

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    final Logger logger = Logger.getLogger(ZipkinServiceHiApplication.class.getName());

    @RequestMapping("/hi")
    public String callHome() {
        logger.info("calling tracing server-hi...");
        return restTemplate.getForObject("http://localhost:8989/hei", String.class);
    }

    @RequestMapping("/info")
    public String info() {
        logger.info("calling tracing server-hi info...");
        return "I am service hi";
    }

    @Bean
    public Sampler defaultSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }
}
