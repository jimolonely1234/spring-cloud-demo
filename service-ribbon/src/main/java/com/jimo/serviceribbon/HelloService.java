package com.jimo.serviceribbon;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.web.client.RestTemplate;

/**
 * @author jimo
 * @code
 * @date 2018/9/6 8:52
 */
@Service
public class HelloService {

    @Autowired
    private RestTemplate restTemplate;

    @HystrixCommand(fallbackMethod = "helloError")
    public String hello(String name) {
        return restTemplate.getForObject("http://EUREKA-CLIENT/?name=" + name, String.class);
    }

    public String helloError(String name) {
        return "sorry," + name + ",error happen";
    }
}
