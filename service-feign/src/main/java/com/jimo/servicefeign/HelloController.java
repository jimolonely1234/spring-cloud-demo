package com.jimo.servicefeign;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author jimo
 * @code
 * @date 2018/9/6 9:43
 */
@RestController
public class HelloController {
    @Autowired
    private SchedualServiceHello schedualServiceHello;

    @GetMapping("/hello")
    public String sayHello(@RequestParam String name) {
        return schedualServiceHello.sayHelloFromClient(name);
    }
}
