package com.jimo.servicefeign;

import org.springframework.stereotype.Component;

/**
 * @author jimo
 * @code 断路时的错误提示
 * @date 2018/9/6 10:19
 */
@Component
public class SchedualServiceHelloHystrix implements SchedualServiceHello {
    @Override
    public String sayHelloFromClient(String name) {
        return "sorry," + name;
    }
}
