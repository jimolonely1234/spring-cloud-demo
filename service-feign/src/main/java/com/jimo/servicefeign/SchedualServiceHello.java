package com.jimo.servicefeign;

import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

/**
 * @author jimo
 * @code
 * @date 2018/9/6 9:30
 */
@FeignClient(value = "eureka-client", fallback = SchedualServiceHelloHystrix.class)
public interface SchedualServiceHello {

    /**
     * @code
     * @author jimo
     * @date 2018/9/6 9:41
     */
    @RequestMapping(value = "/", method = RequestMethod.GET)
    String sayHelloFromClient(@RequestParam(value = "name") String name);
}
