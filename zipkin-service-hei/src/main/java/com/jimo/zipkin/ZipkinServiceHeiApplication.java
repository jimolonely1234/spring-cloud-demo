package com.jimo.zipkin;

import brave.sampler.Sampler;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import java.util.logging.Logger;

/**
 * @code
 * @author jimo
 * @date 2018/9/8 18:58
 */
@SpringBootApplication
@RestController
public class ZipkinServiceHeiApplication {

    public static void main(String[] args) {
        SpringApplication.run(ZipkinServiceHeiApplication.class, args);
    }

    @Autowired
    private RestTemplate restTemplate;

    @Bean
    public RestTemplate restTemplate() {
        return new RestTemplate();
    }

    final Logger logger = Logger.getLogger(ZipkinServiceHeiApplication.class.getName());

    @RequestMapping("/hei")
    public String hei() {
        logger.info("hei is being called");
        return restTemplate.getForObject("http://localhost:8988/info", String.class);
    }

    @RequestMapping("/home")
    public String home() {
        logger.info("home is being called...");
        return "I am service hei";
    }

    @Bean
    public Sampler defaultSampler() {
        return Sampler.ALWAYS_SAMPLE;
    }
}
